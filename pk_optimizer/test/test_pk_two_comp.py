"""
Created on Thu Feb 27 15:06:08 2020

@author: Ethan
"""
import numpy as np
import pytest
import pathlib
from pk_optimizer.pk_two_comp import pk_two_comp

def test_pk1_init():
    """Tests initialization and default values"""

    pk1 = pk_two_comp()
    #Test directory is same as one given
    assert pk1.filename == 'CTPERF005_stress.csv'
    assert pk1.flow == 1/60
    assert pk1.visf == 0.15
    assert pk1.baseline == 60
    assert pk1.perm_surf == 0.35 # Initial concentration of tracer in plasma
    assert pk1.tmax == 6.5 #Time in seconds
    assert pk1.ymax == 250 #Time step
    assert pk1.alpha == 2.5 # Alpha for gamma distribution
    assert pk1.delay == 0

def test_pk1_diff_file():
    """Tests initialization given non-default file"""

    pk2 = pk_two_comp(filename='CTPERF006_stress.csv')
    #Test directory is same as one given
    print(pk2.wd)
    assert pk2.wd == pathlib.Path('./Data')
    assert pk2.filename == 'CTPERF006_stress.csv'
    assert pk2.flow == 1/60
    assert pk2.visf == 0.15
    assert pk2.baseline == 60
    assert pk2.perm_surf == 0.35 # Initial concentration of tracer in plasma
    assert pk2.tmax == 6.5 #Time in seconds
    assert pk2.ymax == 250 #Time step
    assert pk2.alpha == 2.5 # Alpha for gamma distribution
    assert pk2.delay == 0

def test_pkoptimizer_init_badfile():
    """Tests inputting non-string filename"""

    try:
        pk3 = pk_two_comp(3)
    except TypeError:
        assert 'Filename must be a string'
    try:
        pk3 = pk_two_comp([2, 3])
    except TypeError:
        assert 'Filename must be a string'
    try:
        pk3 = pk_two_comp({1:'foo'})
    except TypeError:
        assert 'Filename must be a string'

def test_get_data():
    """Tests reading in data"""

    pk3 = pk_two_comp()
    pk3.get_data(pk3.filename)
    assert pk3.time == [0.0, 2.060608000000002, 4.169921999999929, 6.3469239999999445,
                        8.533386999999948, 10.716613999999936, 12.853088999999954,
                        14.966369999999984, 17.076660999999945, 19.161744,
                        21.249634000000015, 23.319885999999997]
    assert pk3.aorta == [0.0, 79.0, 197.0, 235.0, 253.0, 200.0, 127.0, 70.0, 41.0, 35.0, 36.0, 44.0]
    assert pk3.myo == [62.0, 61.0, 69.0, 91.0, 97.0, 102.0, 104.0, 104.0, 100.0, 95.0, 92.0, 88.0]

def test_dne_file():
    """Tests does not exist file error"""

    pk4 = pk_two_comp(filename = 'ajagea')
    try:
        pk4.main()
    except FileNotFoundError:
        assert "Input file does not exist: {0}. I'll quit now."

def test_output_mse():
    """Tests that the aorta/output MSE is calculated correctly"""

    pk5 = pk_two_comp(filename='CTPERF006_rest.csv')
    pk5.get_data(pk5.filename)

    assert pk5.output_mse([13, 223, 10]) == 100000
    assert pk5.output_mse([-200, 13, 10]) == 100000
    assert pk5.output_mse([123, .33, .20]) == 100000
    assert pk5.output_mse([3, 23, 10]) == 8325.489573778586
    assert pk5.output_mse([13, 223, -10]) == 100000
    assert pk5.output_mse([13, -223, 10]) == 100000
    assert pk5.output_mse([.0013, .62, 50]) == 490.78173062264165

def test_main():
    """Tests main, will calculate all optimal properties correctly"""

    pk6 = pk_two_comp(filename='CTPERF005_rest.csv')
    pk6.main()
    assert pk6.ymax == 403.23
    assert pk6.tmax == 12.43
    assert pk6.alpha == 9.45
    assert pk6.delay == 0
    assert pk6.flow == .0052
    assert pk6.visf == 0.8452
    assert pk6.baseline == 46.5288
    
