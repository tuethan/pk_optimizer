# -*- coding: utf-8 -*-
import numpy as np
import pytest
import pathlib
from pk_optimizer.pk_one_comp import pk_one_comp

"""
Created on Thu Feb 27 15:06:08 2020

@author: Ethan
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


def test_pk1_init():
    """This is a test for initializing the pk1comp class"""

    pk1 = pk_one_comp()
    #Test directory is same as one given
    assert pk1.filename == 'CTPERF005_stress.csv'
    assert pk1.flow == 1/60
    assert pk1.visf == 0.15
    assert pk1.baseline == 60
    assert pk1.perm_surf == 0.35 # Initial concentration of tracer in plasma
    assert pk1.tmax == 6.5 #Time in seconds
    assert pk1.ymax == 250 #Time step
    assert pk1.alpha == 2.5 # Alpha for gamma distribution
    assert pk1.delay == 0

def test_pk1_diff_file():
    """This is a test for initializing the pk1comp class
    with a specific chosen file"""
    pk2 = pk_one_comp(filename='CTPERF005_rest.csv')
    #Test directory is same as one given
    assert pk2.filename == 'CTPERF005_rest.csv'
    assert pk2.flow == 1/60
    assert pk2.visf == 0.15
    assert pk2.baseline == 60
    assert pk2.perm_surf == 0.35 # Initial concentration of tracer in plasma
    assert pk2.tmax == 6.5 #Time in seconds
    assert pk2.ymax == 250 #Time step
    assert pk2.alpha == 2.5 # Alpha for gamma distribution
    assert pk2.delay == 0

def test_pkOptimizer_init_badFile():
    """This is a test for trying to initiate with filename as
    something other than a string"""
    with pytest.raises(Exception):
        pk3 = pk_one_comp(3)
    with pytest.raises(Exception):
        pk3 = pk_one_comp([2, 3])
    with pytest.raises(Exception):
        pk3 = pk_one_comp({1:'foo'})

def test_get_data():
    """This is a test to see if it's properly importing data from csv"""
    pk3 = pk_one_comp()
    pk3.get_data(pk3.filename)
    assert pk3.time == [0.0, 2.060608000000002, 4.169921999999929,
                        6.3469239999999445, 8.533386999999948,
                        10.716613999999936, 12.853088999999954,
                        14.966369999999984, 17.076660999999945,
                        19.161744, 21.249634000000015, 23.319885999999997]
    assert pk3.aorta == [0.0, 79.0, 197.0, 235.0, 253.0, 200.0, 127.0, 70.0, 41.0, 35.0, 36.0, 44.0]
    assert pk3.myo == [62.0, 61.0, 69.0, 91.0, 97.0, 102.0, 104.0, 104.0, 100.0, 95.0, 92.0, 88.0]

def test_dne_file():
    """This is a test to see if it will throw a does not exist error"""
    pk4 = pk_one_comp()
    try:
        pk4.main()
    except FileNotFoundError:
        assert "Input file does not exist: {0}. I'll quit now."

def test_input_mse():
    """Tests to see if calculated MSE is correct"""
    pk5 = pk_one_comp()
    pk5.get_data(pk5.filename)

    assert pk5.input_mse([-5, 1.15, 1/60, 2.3/60]) == 1000000
    assert pk5.input_mse([3, 1.15, 1/60, 0]) == 1000000
    assert pk5.input_mse([255, 3.15, -1/60, 2.3/60]) == 1000000
    assert pk5.input_mse([255, 3.15, 1/60, 2.3/60]) == 20409.552
    assert pk5.input_mse([255, 1.15, 1/60, -2.3/60]) == 1000000
    assert pk5.input_mse([250, 6.5, 3, 2.3/60]) == 516.334

def test_main():
    """Tests to see if optimized parameters are correct"""
    pk6 = pk_one_comp(filename='CTPERF005_rest.csv')
    pk6.main()
    assert pk6.ymax == 403.23
    assert pk6.tmax == 12.43
    assert pk6.alpha == 9.45
    assert pk6.delay == 0
    assert pk6.flow == 8.30000e-03
    assert pk6.vol_plasma == 7.04100e-01
    assert pk6.baseline == 4.74685e+01
    
