{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center> Learning Tool to Explore Parameter Optimization in Perfusion Kinetic Modeling </center>\n",
    "\n",
    "<center>By Ethan Tu</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "<img src=\"https://i.imgur.com/ai4owtM.png\" width=\"100%\">\n",
    "Image from: https://imgur.com/a/euXO2zE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Authors\n",
    "\n",
    "Ethan Tu, Department of Biomedical Engineering, Michigan State University\n",
    "\n",
    "Dr. Dirk Colbry, Department of Computational Mathematics Science and Engineering, Michigan State University\n",
    "\n",
    "Dr. Adam Alessio, Department of Computational Mathematics Science and Engineering, Michigan State University"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Abstract\n",
    "\n",
    "In perfusion kinetic modeling, we're trying to predict the amount of contrast agent that surrounding tissue uptakes from the blood vessel. The gold standard model to predict this uptake is using compartmental models. In compartmental modeling, we treat discrete areas of the body, for example the blood vessel and the surrounding tissue, as separate \"compartments\". Molecules, plasma, red blood cells, contrast agents, etc, can flow freely between compartments at some unique rate determined by multiple parameters. Increasing the amount of compartments increases the complexity and accuracy of the model, but slows down the calculation and prediction. Perfusion modeling is important in the context of CT and MRI imaging where contrast agents are used to enhance captured images. By predicting the amount of contrast agent that leaks outside of the area of interest, we can account for the resulting noise, and optimize the timing of when to take the images. When learning the basics of compartmental modeling, it is usually difficult to instinctively understand what certain parameters do, and how they affect the prediction. The purpose of this project is to provide a learning tool in the form of an interactive GUI that will help visualize the effect of parameters on the compartmental model, and how we can optimize those parameters to fit real world experimental data. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Statement of Need\n",
    "\n",
    "Learning about compartmental modeling in the context of perfusion kinetics is difficult due to the expansive terminology and number of parameters. An interactive tool is needed to show the effect of individual parameters on the input and ouput signal intensity to help students understand the concepts. Additionally, the tool can be used as a research aid to manually find optimal parameters for an individual's data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Installation instructions\n",
    "# How to Run\n",
    "\n",
    "First, set up your conda environment by running the following code:\n",
    "\n",
    "    conda env create --prefix ./envs --file requirements.yml\n",
    "    \n",
    "    conda activate ./envs\n",
    "    \n",
    "Second, run pk_GUI.py in your terminal by switching to the correct directory and running:\n",
    "\n",
    "    python pk_GUI.py\n",
    "    \n",
    "In the popup window, there will be three options:\n",
    "\n",
    "    1) Optimize!\n",
    "    \n",
    "    2) Compare!\n",
    "    \n",
    "    3) Visualize!\n",
    "    \n",
    "Click on each button to go to perform the individual function.\n",
    "\n",
    "    Optimize!\n",
    "        1) From the drop down menu select the file you would like to find optimal parameters for and hit 'Load'\n",
    "        2) Input your guesses for each parameter in the input boxes.\n",
    "        3) Hit the \"Optimize!\" button and it will output the optimized variables\n",
    "    Compare!\n",
    "        1) Select the subject number you'd like to compare resting vs stressed states\n",
    "    Visualize!\n",
    "        1) Select the file you'd like to visualize from drop down menu.\n",
    "        2) Use sliders to adjust the curve and see what the parameter does\n",
    "        \n",
    "# Demonstration\n",
    "See a quick background and demonstration of the GUI in the below video:\n",
    "https://www.youtube.com/watch?v=abWtCTJZioA\n",
    "\n",
    "# Documentation\n",
    "To run autodocumenation of all python files, input in terminal:\n",
    "pdoc --force --html --output-dir ./docs $(MODULENAME)\n",
    "\n",
    "OR\n",
    "\n",
    "make doc\n",
    "\n",
    "To run autodocumentation for a single python file, X, input:\n",
    "pdoc --force --html --output-dir ./docs X.py\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Unit Tests\n",
    "To run unit tests on ALL python files in the pk_optimizer subfolder, run:\n",
    "pytest $(MODULENAME)\n",
    "\n",
    "OR\n",
    "\n",
    "make test\n",
    "\n",
    "To run unit tests on individual files, do:\n",
    "\n",
    "pytest foo.py\n",
    "\n",
    "There will be 3 test documents; import, 1 comp, and 2 comp.\n",
    "Import simply imports all necessary packages and dependencies and makes sure they are imported correctly.\n",
    "Since 1 comp and 2 comp share similar methods and functions, it is redundant to test all the same methods. \n",
    "\n",
    "dne_file test should throw an error when there is no file of that name found. \n",
    "\n",
    "get_data() tests to see if data is properly imported. \n",
    "\n",
    "input_mse() tests if the correct mean squared error value for aorta is returned. \n",
    "\n",
    "output_mse() tests if the correct mean squared error for myocardium is returned.\n",
    "\n",
    "test_main() makes sure optimzed parameters are correctly returned. \n",
    "\n",
    "pk1_diff_file() tests that an input filename is properly imported. \n",
    "\n",
    "pk1_init() tests to see all parameters are properly initialized.\n",
    "\n",
    "init_badFile() throws an error if we try to input a filename with something other than a string, ie 3.1415\n",
    "\n",
    "pk_GUI was not included in pytest because it is simply an interactive GUI. All methods and values returned by the GUI are dependant on the pk_one_comp and pk_two_comp classes.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Example usage\n",
    "\n",
    "Under the 'data' directory, we have provided 2 subjects' data in .csv format. They have a resting and stressed state. Using the GUI, we can load in their data, compare their different states and optimize the necessary parameters to fit a curve to the data. The user may put their own data in .csv format in the data folder and access it using the application. To optimize for Cin or Cout parameters, use the Optimize function in the application. For in depth examples of how the pk_two_comp accomplishes this, refer to the Graph_example.ipynb inside the Examples directory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Methodology\n",
    "\n",
    "Originally, I had intended to implement four different types of compartmental models: 1, 2, tissue homogeneity, and distributed parameter model, and a GUI that would let you select the type of model you'd like to optimize for given a set of data. The original project was more research driven; trying to find not only the optimal parameters for each model, but the optimal type of model that would fit the data. However, the focus changed, and instead of a research focused application, we decided to build a learning tool instead. This encompassed the two basic 1 and 2-compartment models, their sets of parameters for the input function and output function, and trying to visualize what each parameter does. \n",
    "\n",
    "The methodology of implementing these types of models is exactly the same as the original proposal. I used the SciKit function solve_ivp to solve the ODEs for both the 1 and 2 compartment models. The equations I used are the following:\n",
    "For 1 compartment:\n",
    "$$v\\frac{dc}{dt}(t) = \\sum_{i=inlet} F_ic_i(t) - \\sum_{i=outlet} F_oc_(t) $$\n",
    "\n",
    "Two-compartmental model is slightly more complicated. You have two compartments, the plasma and the interstitial fluid (ISF). Since the tracer inside the plasma will flow into the interstitial fluid, we have to define a new constant, the capillary-surface area constant (PS), that will describe the rate of tracer efflux. To account for this new efflux from the *plasma*, we can represent the new ODE as such:\n",
    "\n",
    "$$v_p\\frac{dc_p}{dt}(t) = F_pc_{in}(t) - F_pc_p(t) + PSc_e(t) - PSc_p(t)$$\n",
    "\n",
    "For the ISF compartment, the ODE is:\n",
    "\n",
    "$$v_e\\frac{dc_e}{dt}(t) = PSc_p(t) = PSc_e(t)$$\n",
    "\n",
    "For the input function, which is a gamma variate function, we want to optimize the parameters ymax, tmax, alpha, and delay. Ymax is the amplitude of the input function's peak. Tmax is the time at which ymax appears. Alpha is a scaling parameter. Delay is the time shift if the increase doesn't begin at 0. For the output function, we want to optimize the parameters flow, visf, and baseline. Flow is the blood and plasma flow through the blood vessel. Visf is the volume of the insterstitial fluid. Baseline is the baseline intensity of the surrounding tissue in a CT scan, in Hounsfeld units. Optimizing these variables is done using a Nelder-mead simplex method, which minimizes a cost function (our mean squared error).\n",
    "\n",
    "The last difference between the original proposal and the current project is the GUI itself. Originally, we only wanted one function, the optimization function. Now, since we have a learning tool, we have 3 different ways to play with data. First, we have compare, to directly compare different data points between patients. The optimization funtion still exists but will only solve for 2 compartment models. The new visualize functions allows the user to play with the parameter values and see how it changes the output of our compartmental models. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Concluding Remarks\n",
    "\n",
    "I have to say I'm pretty proud of the results of the project. I'll be honest, I didn't even know the effect of the parameters on the concentration of tracer in the surrounding tissue until I used my own visualize application to see the difference. If I learned something by looking at my own model, then I'm sure others can also find success. \n",
    "\n",
    "Unfortunately, I did not fully complete my GUI. As you may have noticed, I have both a 1 and 2 compartmental class coded. Both have the same functions (input and output MSE and optimization) and both work in the same fashion. They were purposely coded that way to be interchangable when used with the GUI. Originally, the GUI would have a dropdown menu to ask which model you'd like to explore. I never had the change to implement the dropdown menu, so, all models default to the 2-comp. In the future, I hope to not only implement the 1 compartment model with my GUI, but the other types of compartmental model (tissue homogeneity and distributed parameters) I mentioned in my initial proposal.\n",
    "\n",
    "Overall, I've found a lot of enjoyment in the class, and in this project. I plan on using this optimizer for future research with Dr. Alessio, particularly in diagnosing ischemia in coronary arteries using contrast enhanced CT images."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# References\n",
    "\n",
    "A link to the gitlab for cloning the repo: https://gitlab.msu.edu/tuethan/pk_optimizer.git"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
