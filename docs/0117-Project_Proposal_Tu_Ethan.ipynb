{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center>Optimization Tool for CT Tracer-Kinetic Models of Tissue Perfusion</center>\n",
    "\n",
    "<center>By Ethan Tu</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"https://i.imgur.com/2i7upGW.png\" width=\"50%\">\n",
    "<p style=\"text-align: right;\">Image from: https://www.ncbi.nlm.nih.gov/pubmed/23563847</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Overview\n",
    "\n",
    "In dynamic CT imaging, serial CT scans are performed on a region of interest to determine the distribution of indicators and tracers over time. It is generally used to study physiological function of the human body. Such physiological function is directly related to the perfusion and uptake of the tracer in blood vessels and the extravascular space of tissue.$^1$ Therefore, in order to accurately assess functional information of tissue, we must generate a **tracer-kinetic** model. \n",
    "\n",
    "Researchers have developed many different types of tracer-kinetic models, such as one-compartment models, two-compartement exchange models, plug-flow models, tissue homogeneity models, distributed-parameter models, and more. Each aims to predict the same thing - the rate of accumulation of contrast in tissue. Models are solved through a series of differential equations representing the influx and efflux of contrast into the tissue. These differential equations are computationally intensive to solve and have few known variables. Parameters such as diffusion constants, flux, perfusion-surface area product, all need to be determined to optimize the fit of the model to experimental data. Currently, researchers are using machine learning to optimize such parameters, however, I will be using optimization techniques from the scikit package."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Program Description\n",
    "\n",
    "There is a plethora of articles published on tracer-kinetic modeling for iodine perfusion in dynamic CT applications. The scope of this project will be to implement 4 different types of models in python, solve their respective ODEs, and optimize their fit to a set of experimental data we have already collected. Additionally, as an optimization tool, I will create a GUI that will load measured data, present measured data, select model, run optimization, output the fitted model after each iteration of optimization, as well as have sliders than can manipulate individual parameters to see its effect on the model. The proposed models I will include in this project are: 1) one-compartment model (1C), 2) two-compartment exchange model (2CX), 3) tissue homogeneity model (TH), and 4) distributed-parameter model (DP). I will be using differential equations given from literature to develop my models,$^2$ but I will be coding them from scratch. The GUI will also be my own development. Optimization methods from Scikit will be used, modified, and improved to fit my models to the data. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Project Goals and Timeline\n",
    "\n",
    "Below is the proposed project timeline. Bolded dates are due dates for deliverables. Italicized dates are personal goal dates to keep work on track. \n",
    "\n",
    "- **1/17/2020 - Proposal Due**\n",
    "- *1/21/2020 - 1C and 2CX models coded*\n",
    "- *1/28/2020 - TH and DP models coded*\n",
    "- **1/31/2020 - Project git repository and structure due**\n",
    "- *2/04/2020 - Brute force optimal parameters for 1C and 2CX for reference, construct input function*\n",
    "- *2/11/2020 - Use SciKit optimization techniques to for 1C and 2CX*\n",
    "- **2/14/2020 - Stub functions and Example code integration (With documentation)**\n",
    "- *2/18/2020 - Use SciKit optimization techniques for TH and DP models*\n",
    "- *2/25/2020 - Develop GUI skeleton and buttons*\n",
    "- **2/28/2020 - Coding standers and Unit Test integrated into project**\n",
    "- **3/01/2020 - Spring break week**\n",
    "- *3/03/2020 - Refine GUI/Debug code/Streamline code*\n",
    "- *3/10/2020 - Test code with multiple experimental data*\n",
    "- *3/17/2020 - Refine GUI/Debug code/Streamline code*\n",
    "- **3/20/2020 - Code Draft (Code Review 1)**\n",
    "- *3/27/2020 - Refine GUI/Debug code/Streamline code, start working on deliverables*\n",
    "- **4/03/2020 - Code Draft (Code Review 2)**\n",
    "- *4/10/2020 - Refine GUI/Debug code/Streamline code, continue working on deliverables*\n",
    "- *4/17/2020 - Finish deliverables*\n",
    "- **4/20/2020 - Final Project and Presentation Due**\n",
    "\n",
    "Overall, my short-term goals is to code 1C, 2CX, TH, and DP models into python and have them run given an ideal, contrived input function. My mid-term goal is to determine optimal parameters to best fit each model given a set of real-world data and input function. One of the biggest assumptions about these models is that each compartment is considered \"well mixed\". That is, there is no concentration gradient within the lumen or extravascular space. A long-term goal would to be develop my own, unique model which takes into account active transport of the basement membrane and spatial considerations (concentration gradients)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Anticipating Challenges  \n",
    "\n",
    "Key skills I need to learn include solving ODEs in python, GUI construction in python, and optimization of parameters. I have solved ODEs before in matlab, but I have heard from my advisor ODEs in python is a bit trickier. Likewise, I have experience in making GUIs  in java, but not python. I do not expect these challenges to be too difficult to overcome since the theory and background is the same across languages. However, optimization is completely new to me. Besides the few general methods we have gone over in class, I don't know how any optimization method works or how to implement them with regards to model fitting. This will definitely be the biggest challenge for me. I hope to learn more about optimization methods both from my advisor, Dr. Alessio, and online in workable examples. \n",
    "\n",
    "Time management has long been one of my weaknessess. I believe that having biweekly reports and deliverables is great, especially for me who needs deadlines to get things done. The biggest worry I have, however, is underestimation of the scope of my project. In my timeline, I listed personal goals every week. I do not know if these goals are achievable because I have no sense of the scale or difficulty of the project. *If* I find that the workload is too large, I might cut down on the number of models (from 4 to 3), or simply compare different optimization methods for just 1C and 2CX models. Since the scope of this project can change, I will adjust it accordingly. I do not expect this project to deviate from python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# References\n",
    "\n",
    "[1] T.-Y. Lee, “Functional CT: physiological models,” Trends in Biotechnology, vol. 20, no. 8, pp. S3–S10, Aug. 2002, doi: 10.1016/S0167-7799(02)02035-8.\n",
    "\n",
    "[2] M. Ingrisch and S. Sourbron, “Tracer-kinetic modeling of dynamic contrast-enhanced MRI and CT: a primer,” J Pharmacokinet Pharmacodyn, vol. 40, no. 3, pp. 281–300, Jun. 2013, doi: 10.1007/s10928-013-9315-3.\n"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
