{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center>Using Optimization in Dynamic Contrast-Enhanced Perfusion Kinetics</center>\n",
    "\n",
    "<center>by Ethan Tu</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dynamic Contrast-Enhanced (DCE) imaging techniques aim to assess and characterize vasculature of the body by relating tracer-perfusion kinetic models to measured concentrations by MRI or CT. By properly fitting our model to the experimental data, we can determine the optimal time to take readings, maximizing contrast, and minimizing noise.$^1$ Fitting a model to experimental data is an optimization problem. In perfusion kinetics, there are several unknown constants that vary slightly between people. We can tailor (optimize) a model specifically to a person by altering these constants. Additionally, optimizations can be made to the acqusition of the experimental data itself, such as finding optimal temporal resolution, spatial resolution, acquisition time, and spatial coverage.$^2$ In this summary, I will be discussing one major optimization topic in this field: the process of choosing an optimal fit function.\n",
    "\n",
    "Fit functions are a mathematical description and representation of the behavior of a system. Fit functions exist in every field and are used whenever there is a need to predict an outcome. Fit functions can be as simple as a binary distribution, only modeling two possible outcomes. In the context of DCE perfusion kinetics, we want to model the behavior of a tracer in the blood vessel. The tracer is used to enhance contrast between the blood and surrounding tissue and is injected intravenously. In order to properly model the behavior of the tracer after its injection, we must first properly model the injection itself, which is known as the input function.$^3$ Modeling the speed, duration, and volume of the injection is key for the rest of our model to respond to. The question then becomes, what type of distribution, or fit, best represents the injection?\n",
    "\n",
    "A best fit model can be determined using the least squares method. Least squares minimizes the sum of the squares of the residuals made in the results of every single equation.$^4$ Basically, it is a calculation of how far off a line is from a collection of data points. We can use this method of least squares to determine the type of distribution that would best fit the curve of an injection. The behavior of an injection can be broken down to the following sections: a brief period of no flow, an immediate spike of high flow, and then a quick but gradual decrease in flow until it reaches 0. Visually, it might look like the graph below.\n",
    "<img src=\"https://www.gnu.org/software/gsl/doc/html/_images/rand-gaussian-tail.png\" width=\"50%\">\n",
    "<p style=\"text-align: right;\">Image from: https://www.gnu.org/software/gsl/doc/html/randist.html</p>\n",
    "\n",
    "However, this graph is too ideal. In a biological system, such drastic changes and changes in slope is impossible, and therefore this distribution is a poor fit for our data. So what exactly does the data look like? Experimentally, we can measure the injection, which would yield data such as the graph below. The dots in blue are the data points we have collected and the red line is an approximate fit that runs through all the data points.\n",
    "\n",
    "<img src=\"https://www.researchgate.net/profile/Chuan_Zhi_Foo/publication/5399023/figure/fig1/AS:277844522487812@1443254704366/The-simulated-arterial-input-function-AIF-used-in-this-study-was-obtained-by-fitting.png\" width=\"50%\">\n",
    "<p style=\"text-align: right;\">Image from:https://www.ncbi.nlm.nih.gov/pubmed/18454289</p>\n",
    "\n",
    "We see that the same three basic elements of our visualization remains the same; a brief period of no flow, an immediate spike of high flow, and then a quick but gradual decrease in flow until it reaches 0. However, visually, an F-distribution, a Gamma variate distribution, or a skewed Gaussian distribution all might fit the data. We ask ourselves again the main question; which one of these distributions best fits the data? We use the least squares method. For each one of the distributions, we can optimize their fit to the data by changing the variables of their respective equations. An optimal set of parameters is found when their least squares number is the smallest possible. There are numerous algorithms for optimizing variables to fit a data set. Most commonly, we have gradient search, Gauss-Newton, brute force, and Hooke-Jeeves method.$^5$ For DCE perfusion kinetics, the simplex method is the most commonly used method.$^6$ After we find each distribution's best fit, we can find the smallest least square value between the distributions which would determine the best fit distribution.\n",
    "\n",
    "The issue of optimal fit is not only limited to the input function. The same principles can be applied to finding representations of the tissue response, ie. uptake and diffusion. Purely finding the optimal function is also just one of the considerations of a model as a whole. With increasing complexity of a model usually means a better fit. However, more complexity means slower run times, more computing power, and more potential for something to go wrong. Therefore, it is extremely difficult to have an optimal fit for all cases."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# References\n",
    "\n",
    "[1]M. Ingrisch and S. Sourbron, “Tracer-kinetic modeling of dynamic contrast-enhanced MRI and CT: a primer,” J Pharmacokinet Pharmacodyn, vol. 40, no. 3, pp. 281–300, Jun. 2013, doi: 10.1007/s10928-013-9315-3.\n",
    "\n",
    "[2]K. S. Lawrence et al., “Kinetic model optimization for characterizing tumour physiology by dynamic contrast-enhanced near-infrared spectroscopy,” Phys. Med. Biol., vol. 58, no. 5, pp. 1591–1604, Feb. 2013, doi: 10.1088/0031-9155/58/5/1591.\n",
    "\n",
    "[3]M. Bindschadler, D. Modgil, K. R. Branch, P. J. L. Riviere, and A. M. Alessio, “Comparison of blood flow models and acquisitions for quantitative myocardial perfusion estimation from dynamic CT,” Phys. Med. Biol., vol. 59, no. 7, pp. 1533–1556, Mar. 2014, doi: 10.1088/0031-9155/59/7/1533.\n",
    "\n",
    "[4]G. Glatting, P. Kletting, S. N. Reske, K. Hohl, and C. Ring, “Choosing the optimal fit function: Comparison of the Akaike information criterion and the F-test,” Medical Physics, vol. 34, no. 11, pp. 4285–4292, Nov. 2007, doi: 10.1118/1.2794176.\n",
    "\n",
    "[5]K. Madsen and H. Nielsen, Introduction to Optimization and Data Fitting. Technical University of Denmark, 2010.\n",
    "\n",
    "[6]T. Y. Lee, “Functional CT: physiological models,” Trends in Biotechnology, vol. 20, no. 8, pp. S3–S10, Aug. 2002, doi: 10.1016/S0167-7799(02)02035-8."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
