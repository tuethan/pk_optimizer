"""This is just to test if all necessary imports
are imported correctly"""

import pathlib
import os
import csv
import re
import math
import glob
import tkinter as tk
import numpy as np
import scipy
import matplotlib.pyplot as plt

def test_imports():
    """Basic code using all imported packages"""

    time = []
    aorta = []
    myo = []

    array_np = np.array([0, 1, 2])
    assert isinstance(array_np, np.ndarray) == True
    filenames = glob.glob('*.{}'.format('csv'))
    assert filenames == []

    filename = pathlib.Path('./Data/CTPERF005_stress.csv')

    data = list(csv.reader(open(str(filename)), delimiter='\t'))

    for i in range(12):
        time.append(
            float(re.compile(r'\d+[.]+\d+|\d+').findall(data[i+1][0])[0]))
        aorta.append(
            float(re.compile(r'\d+[.]+\d+|\d+').findall(data[i+1][1])[0]))
        myo.append(
            float(re.compile(r'\d+[.]+\d+|\d+').findall(data[i+1][2])[0]))
    assert time == [0.0, 2.060608000000002, 4.169921999999929,
                    6.3469239999999445, 8.533386999999948,
                    10.716613999999936, 12.853088999999954,
                    14.966369999999984, 17.076660999999945,
                    19.161744, 21.249634000000015, 23.319885999999997]
    assert aorta == [0.0, 79.0, 197.0, 235.0, 253.0, 200.0, 127.0, 70.0, 41.0, 35.0, 36.0, 44.0]
    assert myo == [62.0, 61.0, 69.0, 91.0, 97.0, 102.0, 104.0, 104.0, 100.0, 95.0, 92.0, 88.0]
    plt.plot(time, aorta)

    math_test = math.exp(3)
    assert math_test == math.exp(3)
    
