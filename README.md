---
# Program Description

There is a plethora of articles published on tracer-kinetic modeling for iodine perfusion in dynamic CT applications. The scope of this project will be to implement 4 different types of models in python, solve their respective ODEs, and optimize their fit to a set of experimental data we have already collected. Additionally, as an optimization tool, I will create a GUI that will load measured data, present measured data, select model, run optimization, output the fitted model after each iteration of optimization, as well as have sliders than can manipulate individual parameters to see its effect on the model. The proposed models I will include in this project are: 1) one-compartment model (1C), 2) two-compartment exchange model (2CX), 3) tissue homogeneity model (TH), and 4) distributed-parameter model (DP). I will be using differential equations given from literature to develop my models,$^2$ but I will be coding them from scratch. The GUI will also be my own development. Optimization methods from Scikit will be used, modified, and improved to fit my models to the data. 

# How to Run

First, set up your conda environment by running the following code:

    conda env create --prefix ./envs --file requirements.yml
    
    conda activate ./envs
    
Second, run pk_GUI.py in your terminal by running:

    ./pk_GUI.py
    
In the popup window, there will be three options:

    1) Optimize!
    
    2) Compare!
    
    3) Visualize!
    
Click on each button to go to perform the individual function.

    Optimize!
        1) From the drop down menu select the file you would like to find optimal parameters for and hit 'Load'
        2) Input your guesses for each parameter in the input boxes.
        3) Hit the "Optimize!" button and it will output the optimized variables
    Compare!
        1) Select the subject number you'd like to compare resting vs stressed states
    Visualize!
        1) Select the file you'd like to visualize from drop down menu.
        2) Use sliders to adjust the curve and see what the parameter does
#Demonstration
See a quick background and demonstration of the GUI in the below video:
https://www.youtube.com/watch?v=abWtCTJZioA